# k8s-monitoring

## Prerequisites

Install [jsonnet-bundler](https://github.com/jsonnet-bundler/jsonnet-bundler) and [jsonnet](https://github.com/google/jsonnet) locally, and then install the [kube-prometheus](https://github.com/prometheus-operator/kube-prometheus) vendored manifests (e.g. `jb install github.com/prometheus-operator/kube-prometheus/jsonnet/kube-prometheus@main`).

Alternatively, follow the instructions below on how to build a docker image containing the required tools and install the vendored manifests through the accompanying Makefile.

### Build tools and vendor installation

```
# build the docker image containing tools used by this project
make tooling

# install kube-prometheus vendored/library manifests
make vendor_install
```

### Kind cluster for local development

Install [Kind](https://kind.sigs.k8s.io/) and then create a Kind cluster

```
kind create cluster
```

## Transpile Manifests

```
make build
make k_apply
```

## Teardown Resources

```
make k_delete
```
