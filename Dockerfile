FROM golang:1.22

ENV JSONNET_VERSION v0.20.0
ENV JB_VERSION v0.5.1
ENV GOJSONTOYAML_VERSION latest

RUN go install github.com/jsonnet-bundler/jsonnet-bundler/cmd/jb@${JB_VERSION}
RUN go install github.com/google/go-jsonnet/cmd/jsonnet@${JSONNET_VERSION}
RUN go install github.com/brancz/gojsontoyaml@${GOJSONTOYAML_VERSION}
