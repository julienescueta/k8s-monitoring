.ONESHELL:
.SHELLFLAGS := -eu -o pipefail -c
.DELETE_ON_ERROR:
MAKEFLAGS += --warn-undefined-variables
MAKEFLAGS += --no-builtin-rules

TOOLING_IMAGE_NAME = jsonnet-ci
TOOLING_IMAGE_VERSION = latest
TOOLING_IMAGE = $(TOOLING_IMAGE_NAME):$(TOOLING_IMAGE_VERSION)
KUBE_PROMETHEUS_VERSION = main

.PHONY: build
build: tooling vendor_install
	docker run --rm -v $(CURDIR):/work --workdir /work $(TOOLING_IMAGE) ./build.sh ./example.jsonnet

.PHONY: clean
clean:
	sudo rm -vrf manifests/ vendor/

.PHONY: k_apply
k_apply: manifests/
	kubectl apply --server-side -f manifests/setup
	kubectl wait \
		--for condition=Established \
		--all CustomResourceDefinition \
		--namespace=monitoring
	kubectl apply -f manifests/

.PHONY: k_delete
k_delete:
	kubectl delete --ignore-not-found=true -f manifests/ -f manifests/setup

.PHONY: tooling
tooling:
	docker build --tag $(TOOLING_IMAGE_NAME) .

.PHONY: vendor_install
vendor_install: tooling
	docker run --rm -v $(CURDIR):/work --workdir /work $(TOOLING_IMAGE) jb install github.com/prometheus-operator/kube-prometheus/jsonnet/kube-prometheus@$(KUBE_PROMETHEUS_VERSION)
